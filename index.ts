import * as stringify from 'fast-stable-stringify'
import { shardusFactory } from '@shardus/core'
import * as crypto from '@shardus/crypto-utils'

// TODO
// * Include diffing algorithm so we only call set on state that has changed.
//   This would make the app a lot more efficient over the network,
//   however in a sharded network we wouldn't always have access to every slice
//   of the user's state.

crypto.init('abcd123abcd123abcdef3123456e5083934424abcfab9eee8765423111111111')

export type Shardus = ReturnType<typeof shardusFactory>

export type TimeStamp          = number
export type HexString          = string // object ids, hash values
export type StringifiedAccount = string // JSON stringified object

export type WrappedAccount<S> = {
  data: Account<S>
} & ReturnType<Shardus['createWrappedResponse']>

export type WrappedAccounts<S> = { [accountId: HexString]: WrappedAccount<S> }

export type KeyResult = {
  id: HexString
  timestamp: TimeStamp
  keys: {
    sourceKeys: HexString[]
    targetKeys: HexString[]
    allKeys: HexString[]
    timestamp: TimeStamp
  }
}

export type ApplyResponse = ReturnType<Shardus['createApplyResponse']>

export type ValidateResponse = {
  success: boolean
  reason: string
}

type StateKey = string | number | symbol

export type Account<S> = {
  id: HexString        // 32 byte hex string
  timestamp: TimeStamp // ms since epoch
  state: S
  stateKey: StateKey // When we break up the incoming object into accounts, this is what the og key was
}

export type Accounts<S> = {
  [id: HexString]: Account<S>
}

export type Transaction<S> = {
  accounts: Accounts<ValuesOf<S>>
  timestamp: TimeStamp
}

type ValuesOf<T> = T[keyof T]

const GLOBAL_ACCOUNT_ADDRESS: HexString = 'a'.repeat(64)

const createAccount = <S>(accountNumber: HexString, state?: ValuesOf<S>, stateKey?: StateKey): Account<ValuesOf<S>> => {
  return {
    id: accountNumber,
    timestamp: Date.now(),
    state: state,
    stateKey: stateKey
  }
}

// We need a stable way to derive the accountId from the object key aka stateKey
// so we don't create new accounts every time our user makes a save to the db.
// Also include an optional prefix to tack onto the beginning of the result,
// which will allow the colocation of data.
const objectKeyToAccountId = (key: string, prefix?: HexString): HexString => {
  let hash = crypto.hash(key)

  if (prefix) {
    hash = hash.slice(prefix.length)
    hash = prefix + hash
  }

  return hash
}

const objectToAccounts = <S>(object: S, colocations: Colocations<S>): Accounts<ValuesOf<S>> => {
  const accounts: Accounts<ValuesOf<S>> = {}

  for (const key in object) {
    const accountId = objectKeyToAccountId(key, colocations[key])
    accounts[accountId] = createAccount(accountId, object[key], key)
  }

  return accounts
}

const accountsToObject = <S>(accounts: Accounts<ValuesOf<S>>): S => {
  let object: S = {} as S

  for (const accountId in accounts) {
    const account = accounts[accountId]
    object[account.stateKey] = account.state
  }

  return object
}

type Colocations<S> = { [stateField in keyof S]?: ColocationKey }
type ColocationKey = string

interface DB<S extends {}, GS> {
  get(): S
  set(val: Partial<S>): ValidateResponse
  getGlobal(): any
  setGlobal(val: any): ValidateResponse
  colocations: Colocations<S>
  shardus: Shardus
  crypto: typeof crypto
  _accounts: Accounts<ValuesOf<S>>
}

// See the idea here is to just have a single account with all the state.
export default function shardusDB<S, GS = null>(): DB<S, GS> {
  const shardus = shardusFactory({
    server: {
      p2p: {
        minNodesToAllowTxs: 1,
        cycleDuration: 5,
        queryDelay: 5,
      },
    },
  })

  const hashAccount = (account: Account<any>): HexString => crypto.hashObj(account)
  const hashTx      = (tx: Transaction<any>):  HexString => crypto.hashObj(tx)

  // Database right here boys and girls
  const accounts: Accounts<ValuesOf<S>> = {}
  let globalState: GS = null // TODO I need a primer on global account usage, then design this

  const set = (state: Partial<S>): ValidateResponse => {
    const txAccounts = objectToAccounts(state, db.colocations)
    const tx: Transaction<typeof state> = {
      timestamp: Date.now(),
      accounts: txAccounts
    }
    return shardus.put(tx)
  }

  const get = () => {
    return accountsToObject<S>(accounts)
  }

  const setGlobal = (gs: GS): ValidateResponse => {
    globalState = gs
    return {
      success: true,
      reason: 'Couldnt fail!'
    }
    // return shardus.setGlobal(
    //   GLOBAL_ACCOUNT_ADDRESS,
    //   globalState,
    //   Date.now(),
    //   GLOBAL_ACCOUNT_ADDRESS
    // )
  }

  const getGlobal = () => {
    return globalState
  }

  shardus.setup({

    validate(tx: Transaction<S>): ValidateResponse {
      return {
        success: true,
        reason: 'Because this is an internal design consideration of shardusDB. :)',
      }
    },

    apply(tx: Transaction<S>, wrappedAccounts: WrappedAccounts<ValuesOf<S>>): ApplyResponse {
      for (const accountId in tx.accounts) {
        const txAccount = tx.accounts[accountId]
        const account = wrappedAccounts[accountId].data

        account.state = txAccount.state
        account.stateKey = txAccount.stateKey
        account.timestamp = tx.timestamp
      }

      return shardus.createApplyResponse(hashTx(tx), tx.timestamp)
    },

    crack(tx: Transaction<S>): KeyResult {
      const keys = Object.keys(tx.accounts)

      return {
        id: hashTx(tx),
        timestamp: tx.timestamp,
        keys: {
          sourceKeys: keys,
          targetKeys: keys,
          allKeys: keys,
          timestamp: tx.timestamp
        }
      }
    },

    setAccountData (accountsToSet: Account<ValuesOf<S>>[]): void {
      accountsToSet.forEach(account => accounts[account.id] = account)
    },

    resetAccountData(accountBackupCopies: Account<ValuesOf<S>>[]) {
      this.setAccountData(accountBackupCopies)
    },

    deleteAccountData(addressList: HexString[]) {
      for (const address of addressList) delete accounts[address]
    },

    deleteLocalAccountData() {
      for (const id in accounts) delete accounts[id]
    },

    updateAccountFull(wrappedAccount: WrappedAccount<ValuesOf<S>>, localCache, applyResponse: ApplyResponse): void {
      const { accountId, accountCreated, data: updatedAccount } = wrappedAccount

      const ogAccount = accounts[accountId]
      const hashBefore = ogAccount ? hashAccount(ogAccount) : ''
      const hashAfter = hashAccount(updatedAccount)

      accounts[accountId] = updatedAccount

      shardus.applyResponseAddState(
        applyResponse,
        updatedAccount,
        localCache,
        accountId,
        applyResponse.txId,
        applyResponse.txTimestamp,
        hashBefore,
        hashAfter,
        accountCreated
      )
    },

    updateAccountPartial(wrappedData, localCache, applyResponse): void {
      this.updateAccountFull(wrappedData, localCache, applyResponse)
    },

    getRelevantData (accountId: HexString, tx: Transaction<S>): WrappedAccount<ValuesOf<S>> {
      let account = accounts[accountId]
      let accountCreated = false
      const txAccount = tx.accounts[accountId]

      if (!account) {
        account = createAccount(accountId, txAccount.state, txAccount.stateKey)
        accountCreated = true
      }

      const accountHash = hashAccount(account)

      const wrappedAccount = shardus.createWrappedResponse(
        accountId,
        accountCreated,
        accountHash,
        account.timestamp,
        account
      )

      return wrappedAccount
    },

    getAccountData (accountStart: HexString, accountEnd: HexString, maxRecords: number) {
      const wrappedAccounts: WrappedAccount<ValuesOf<S>>[] = []

      // We'll parse these for later numerical comparisons
      const start = parseInt(accountStart, 16)
      const end = parseInt(accountEnd, 16)

      for (const account of Object.values(accounts)) {
        const accountId = parseInt(account.id, 16)
        if (accountId < start || accountId > end) continue

        const wrappedAccount = shardus.createWrappedResponse(
          accountId,
          false,
          hashAccount(account),
          account.timestamp,
          account
        )

        wrappedAccounts.push(wrappedAccount)
        if (wrappedAccounts.length >= maxRecords) return wrappedAccounts
      }

      return wrappedAccounts
    },

    getAccountDataByRange(accountStart, accountEnd, tStart, tEnd, maxRecords) {
      const wrappedAccounts: WrappedAccount<ValuesOf<S>>[] = []
      const start = parseInt(accountStart, 16)
      const end = parseInt(accountEnd, 16)

      for (const account of Object.values(accounts)) {

        // Skip if not in account id range
        const id = parseInt(account.id, 16)
        if (id < start || id > end) continue

        // Skip if not in timestamp range
        const timestamp = account.timestamp
        if (timestamp < tStart || timestamp > tEnd) continue

        const wrappedAccount = shardus.createWrappedResponse(
          account.id,
          false,
          hashAccount(account),
          account.timestamp,
          account
        )

        wrappedAccounts.push(wrappedAccount)

        // Return results early if maxRecords reached
        if (wrappedAccounts.length >= maxRecords) return wrappedAccounts
      }

      return wrappedAccounts
    },

    getAccountDataByList(addressList: HexString[]): WrappedAccount<ValuesOf<S>>[] {
      let wrappedAccounts: WrappedAccount<ValuesOf<S>>[] = []

      for (const address of addressList) {
        const account = accounts[address]

        if (!account) continue

        const wrappedAccount = shardus.createWrappedResponse(
          account.id,
          false,
          hashAccount(account),
          account.timestamp,
          account
        )

        wrappedAccounts.push(wrappedAccount)
      }

      return wrappedAccounts
    },

    calculateAccountHash(account: Account<S>): HexString {
      return hashAccount(account)
    },

    // This allows you to serialize your account into a string for later viewing
    // within the logs.
    getAccountDebugValue(wrappedAccount: WrappedAccount<S>): string {
      return `${stringify(wrappedAccount.data)}`
    },

    close() { console.log('Shutting down...') },
  })

  shardus.registerExceptionHandler()
  shardus.start()

  const db = {
    get,
    set,
    getGlobal,
    setGlobal,
    colocations: {},
    shardus,
    crypto,
    _accounts: accounts
  }

  return db
}
