import { shardusFactory } from '@shardus/core';
import * as crypto from '@shardus/crypto-utils';
export declare type Shardus = ReturnType<typeof shardusFactory>;
export declare type TimeStamp = number;
export declare type HexString = string;
export declare type StringifiedAccount = string;
export declare type WrappedAccount<S> = {
    data: Account<S>;
} & ReturnType<Shardus['createWrappedResponse']>;
export declare type WrappedAccounts<S> = {
    [accountId: HexString]: WrappedAccount<S>;
};
export declare type KeyResult = {
    id: HexString;
    timestamp: TimeStamp;
    keys: {
        sourceKeys: HexString[];
        targetKeys: HexString[];
        allKeys: HexString[];
        timestamp: TimeStamp;
    };
};
export declare type ApplyResponse = ReturnType<Shardus['createApplyResponse']>;
export declare type ValidateResponse = {
    success: boolean;
    reason: string;
};
declare type StateKey = string | number | symbol;
export declare type Account<S> = {
    id: HexString;
    timestamp: TimeStamp;
    state: S;
    stateKey: StateKey;
};
export declare type Accounts<S> = {
    [id: HexString]: Account<S>;
};
export declare type Transaction<S> = {
    accounts: Accounts<ValuesOf<S>>;
    timestamp: TimeStamp;
};
declare type ValuesOf<T> = T[keyof T];
declare type Colocations<S> = {
    [stateField in keyof S]?: ColocationKey;
};
declare type ColocationKey = string;
interface DB<S extends {}, GS> {
    get(): S;
    set(val: Partial<S>): ValidateResponse;
    getGlobal(): any;
    setGlobal(val: any): ValidateResponse;
    colocations: Colocations<S>;
    shardus: Shardus;
    crypto: typeof crypto;
    _accounts: Accounts<ValuesOf<S>>;
}
export default function shardusDB<S, GS = null>(): DB<S, GS>;
export {};
